package com.groundgurus.javaapplication1;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Xidd
 */
public class ExceptionExample2 {
    public static void main(String[] args) {
        try {
            Scanner in = new Scanner(System.in);
            System.out.print("Enter first number: ");
            int firstNumber = in.nextInt();
            System.out.print("Enter second number: ");
            int secondNumber = in.nextInt();


            int sum = firstNumber + secondNumber;
            System.out.println("The sum is " + sum);
        } catch (Exception e) {
            System.out.println("Oops, you weren't suppose to type that! Try again");
        }

    }
}
