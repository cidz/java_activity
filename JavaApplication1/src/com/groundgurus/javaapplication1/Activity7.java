package com.groundgurus.javaapplication1;

/**
 *
 * @author Xidd
 */
import java.io.File;
import java.util.Properties;

public class Activity7 {
    public static void main(String[] args) {
//           File myFile = new File("");
//        Properties properties = System.getProperties();
//        properties.list 
           
            Properties System.getProperties();
            
            File myFile = new File(System.getProperty("user.home") +
                  File.separator + "Documents" + File.separator + "dantes_inferno.txt");
//            System.out.println();
            System.out.println("Hidden : " + myFile.isHidden());
            System.out.println("Directory : " + myFile.isDirectory());
            System.out.println("File : " + myFile.isFile());
            System.out.println("Absolute Path : " + myFile.getAbsoluteFile());
    }
}
