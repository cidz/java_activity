package com.groundgurus.javaapplication1;

/**
 *
 * @author Xidd
 */
public class ExceptionExample {
    public static void main(String[] args) {
        int[] array = {10, 20, 30, 40, 50};
        
        try {
            System.out.println(array[-5]);
        }catch(Exception ex){
            System.out.println("Error was handled");
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            
        }
        
    }
}
