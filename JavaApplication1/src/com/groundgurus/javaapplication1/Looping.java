package com.groundgurus.javaapplication1;

/**
 * Create a program that accepts an input and prints out from 1 to input
 * @author Xidd
 */
public class Looping {
    public static void main(String[] args) {
        
        int input = 10;
        
        for (int i = 1; i <= input; i++){
             System.out.println(i);            
        }
    }
}
