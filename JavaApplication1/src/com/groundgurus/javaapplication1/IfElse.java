package com.groundgurus.javaapplication1;

/**
 *
 * @author Xidd
 */
public class IfElse {
    public static void main(String[] args) {

        String username = "john.doe";
        String password = "groundgurus";
        
        if(username.equals("john.doe") && password.equals("groundgurussss")){
            System.out.println("You are authorized");
        }else{
            System.out.println("Get out of town");
        }
    }
}

