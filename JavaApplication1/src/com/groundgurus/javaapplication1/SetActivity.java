package com.groundgurus.javaapplication1;

/**
 *
 * @author Xidd
 */
import java.util.HashSet;
import java.util.Set;
public class SetExample {
      public static void main(String[] args) {
            Set<String> mySet = new HashSet<>();
            mySet.add("Hello");
            mySet.add("Hi");
            mySet.add("Hello");

//            System.out.println(mySet);

            for (String str : mySet){
                System.out.println(str.toUpperCase());
            }
      }
}