package com.groundgurus.javaapplication1;

/**
 *
 * @author Xidd
 */
public class ArraySample {
    public static void main(String[] args) {
        int numbers[] = {10, 100, 20, 200, 30, 300, 40, 400};
        
        for(int i = 0; i < numbers.length; i++){
            System.out.println(numbers[i]);
        }
    }
}
