package com.groundgurus.javaapplication1;

/**
 *
 * @author Xidd
 */

import java.util.ArrayList;
import java.util.List;

public class ListExample {
     public static void main(String[] args) {
         List<String> myList = new ArrayList<>();
         // myList.add(1); error string only accept
         myList.add("Hello");
         myList.add("Hello");
         myList.add("Hi");
         myList.add("Howdy");
         System.out.println(myList);
        System.out.println(myList.get(2));
        
//        Set<String> mySet = new HashSet<>();
//         
//         mySet.add("Hello");
//         mySet.add("Hello");
//         mySet.add("Hi");
//         mySet.add("Howdy");
//         System.out.println(mySet);
//        System.out.println(mySet.get(2));
        
        //for loop
        for (int i =0; i< myList.size(); i++ ){
            System.out.println(myList.get(i));
        }
        
        //for each
        for (String greeting: myList){
            System.out.println(greeting);
        }
        
        //java 8        
        myList.forEach(System.out::println);
            
       
     }
}