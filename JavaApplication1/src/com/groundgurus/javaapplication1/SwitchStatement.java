package com.groundgurus.javaapplication1;

/**
 *
 * @author Xidd
 */
public class SwitchStatement {
    public static void main(String[] args) {
        int roleId = 1;
        
        switch (roleId) {
            case 1:
                System.out.println("Admin");
                break;
            case 2:
                System.out.println("User");
                break;
            case 3:
                System.out.println("Super Admin");
                break;
            default:
                System.out.println("Anonymous");
                break;
        }
    }
}
