package com.groundgurus.javaapplication1;

/**
 *
 * @author Xidd
 */
import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter First Number: ");
        double firstNumber = sc.nextDouble();
        System.out.println("Enter Second Number: ");
        double secondNumber = sc.nextDouble();
        
        double sum = firstNumber + secondNumber;
        System.out.println("The sum is " + sum);
        
        double diff = firstNumber - secondNumber;
        System.out.println("The difference is " + diff);
        
        double product = firstNumber * secondNumber;
        System.out.println("The product is " + product);
        
        double quotient = firstNumber / secondNumber;
        System.out.println("The quotient is " + quotient);
        System.out.println("The quotient is " + quotient);
    }
}
