package com.groundgurus.javaapplication1;

/**
 *
 * @author Xidd
 */
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Activity2 {
    public static void main(String[] args) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
        System.out.println("Today is " + sdf.format(new Date()));
        
        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        System.out.println("Today is " + df.format(new Date()));
    }
}
