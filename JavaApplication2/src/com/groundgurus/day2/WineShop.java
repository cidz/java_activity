package com.groundgurus.day2;

/**
 *
 * @author Xidd
 */
public class WineShop extends Shop{
    private boolean areMinorsAllowed;

    public WineShop(boolean areMinorsAllowed) {
        this.areMinorsAllowed = areMinorsAllowed;
    }

    public WineShop(boolean areMinorsAllowed, String name, String[] items, String address, int numberOfEmployees) {
        super(name, items, address, numberOfEmployees);
        this.areMinorsAllowed = areMinorsAllowed;
    }

    public boolean isAreMinorsAllowed() {
        return areMinorsAllowed;
    }

    public void setAreMinorsAllowed(boolean areMinorsAllowed) {
        this.areMinorsAllowed = areMinorsAllowed;
    }

    @Override  // optional
    public void printDetails() {
        super.printDetails(); 
        System.out.println("Are Minors Allowed : " + areMinorsAllowed );
    }
    
    
    
}
