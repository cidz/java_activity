package com.groundgurus.day2;

/**
 *
 * @author Xidd
 */
public class EmployeeMain {
    public static void main(String[] args) {
        Employee employeeDetails1 = new Employee("john","M", "Smith", "Clerk", "Management",
                                                new Address("123 Acme","","Mandaluyong","Manila","5001"));
        
        employeeDetails1.printDetails();
        
        Employee employeeDetails2 = new Employee("richard","C", "Doe", "Manager", "Management",
                                                new Address("456 Acme","","Mandaluyong loob","pasig","8080"));
        
        employeeDetails2.printDetails();
        
    }
}
