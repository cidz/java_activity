package com.groundgurus.day2;

import java.util.Arrays;

/**
 *
 * @author Xidd
 */
public class AnimalZooMain {
    public static void main(String[] args) {
        AnimalZoo manilaZoo = 
                    new AnimalZoo(Arrays.asList(
                                                new Dog(),
                                                new Dog(),
                                                new Dog(),
                                                new Cat(),
                                                new Cat()                            
                                                )
                    );
        manilaZoo.walkAllAnimals();
    }
}
